<?php
/*
 * Plugin Name: Seans Project Manager
 * Plugin URI: http://seansspace.com
 * Description: Test plugin to learn how to make them
 * Author: Sean
 * Version: 1.0
 * Author URI: http://seansspace.com
 */

//Admin area code
add_action( 'init', 'spm_init_custom_post_types' );
add_action( 'admin_init', 'spm_init_custom_fields' );
add_filter( 'template_include', 'include_template_function' );
add_shortcode( 'project_list', 'show_project_list' );

function spm_init_custom_post_types() {
  $labels = array(
    'name' => _x('Projects', 'post type general name'),
    'singular_name' => _x('Project', 'post type singular name'),
    'add_new' => _x('Add New', 'Project'),
    'add_new_item' => __('Add New Project'),
    'edit_item' => __('Edit Project'),
    'new_item' => __('New Project'),
    'all_items' => __('All Projects'),
    'view_item' => __('View Project'),
    'search_items' => __('Search Projects'),
    'not_found' =>  __('No Projects found'),
    'not_found_in_trash' => __('No Projects found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => 'Projects'
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => array( 'with_front' => false, 'slug' => 'projects' ),
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => true,
    'menu_position' => null,
    'supports' => array( 'title', 'thumbnail', 'comments' )
  );
  register_post_type( 'spm_project', $args );
}

function spm_init_custom_fields() {
  if( function_exists( 'x_add_metadata_group' ) && function_exists( 'x_add_metadata_field' ) ) {
    x_add_metadata_group( 'spm_project', 'spm_project', array(
      'label' => 'Project'
    ));

    /*x_add_metadata_field( 'spm_name', 'spm_project', array(
      'group' => 'spm_project',
      'description' => 'Project name',
      'label' => 'Name',
      'display_column' => true
    ));*/

    x_add_metadata_field( 'spm_description', 'spm_project', array(
      'group' => 'spm_project',
      'description' => 'Project Description',
      'label' => 'Description',
      'field_type' => 'wysiwyg',
      'display_column' => true,
    ));

    x_add_metadata_field( 'spm_version', 'spm_project', array(
      'group' => 'spm_project',
      'description' => 'Version number',
      'label' => 'Version',
      'display_column' => true
    ));

    x_add_metadata_field( 'spm_website', 'spm_project', array(
        'group' => 'spm_project',
        'description' => 'Website of the project',
        'label' => 'Website',
        'display_column' => true
    ));

    x_add_metadata_field( 'spm_download', 'spm_project', array(
        'group' => 'spm_project',
        'description' => 'URL for the latest download',
        'label' => 'Download URL',
        'display_column' => true
    ));

  }
}

function include_template_function( $template_path ) {
    if( get_post_type() == 'spm_project' ) {
        if ( is_single() ) {
            $prefix = 'single';
        }
        else if ( is_archive() || is_search() ) {
            $prefix = 'archive';
        }
        $file_name = $prefix."-spm-project.php";
        if ( $theme_file = locate_template ( array( $file_name ) ) ) {
            $template_path = $theme_file;
        }
        else {
            $template_path = plugin_dir_path( __FILE__ )."templates/".$file_name;
        }
    }
    return $template_path;
}

function show_project_list( $array, $atts ) {
    $projects = get_posts( array (
        'post_type' => 'spm_project',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        //'fields' => 'ids'
    ) );
    var_dump( $projects );
    // for( $x=0; $x < sizeof( $projects ); $x++ ) {
    //   $project = $projects[$x];
    //   print "<div><a href='{$project->guid}'>{$project->post_title}</a><br/> <small>{$project->post_modified_gmt}</small></div>";
    // }
    include plugin_dir_path( __FILE__ ) . "templates/spm-project-list.php";
}
